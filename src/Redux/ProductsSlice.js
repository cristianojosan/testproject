import { createSlice } from "@reduxjs/toolkit";

const ProductsSlice = createSlice({
  name: "products",
  initialState: {
    selectCampus: [

      {
        id: '1234',
        address: 'Nicolae Dimo 1',
        principal: 'Sorocean Ion',
        primary: 'Melisa George',
        name: 'Austin ISD',
      },
      {
        id: '4234',
        address: 'Vasile Alecsandri',
        principal: 'Melnic Grisa',
        primary: 'Melisa Bologan',
        name: 'Lacatel JSX',
      },
      {
        id: '78821',
        address: 'Mihai Eminescu',
        principal: 'Sculea Ion',
        primary: 'Andreea Ceban',
        name: 'Dumbrva SUS',
      }
    ],
    cartItems: [],
    grades: [
      {
        id: 1,
        title: '6th Grade'
      },
      {
        id: 2,
        title: '7th Grade'
      },
      {
        id: 3,
        title: '8th Grade'
      },
      {
        id: 4,
        title: '11th Grade'
      },
      {
        id: 5,
        title: '12th Grade'
      },

    ],
    subjects: [
      {
        id: 1,
        title: 'Social Studies',
        defaultBgColor: 'green',
        activeBtton: 'subject-active-button'
      },
      {
        id: 2,
        title: 'Math',
        defaultBgColor: 'blue',
        activeBtton: 'subject-active-button2'
        
      },
      {
        id: 3,
        title: 'Science',
        defaultBgColor: 'orange',
        activeBtton: 'subject-active-button3'
      },
      {
        id: 4,
        title: 'ELAR',
        defaultBgColor: 'red',
        activeBtton: 'subject-active-button4'
      },
      
    ],
    types: [
      {
        id: 1,
        
      },
      {
        id: 2,
        
      },
      {id: 3,
        
      },
      {id: 4,
        
      },
      {id: 5,
        
      },
      
   
    ],

    products: [
      { id: 1, titleName: " Quizzes1", grade: 1, subject: [1], type: 1 },
      { id: 2, titleName: " Quizzes2", grade: 2, subject: [2], type: 1 },
      { id: 3, titleName: " Quizzes3", grade: 2, subject: [3], type: 1 },
      { id: 4, titleName: " Quizzes4", grade: 3, subject: [4], type: 1 },
      { id: 5, titleName: " Notes1", grade: 1, subject: [4], type: 2 },
      { id: 6, titleName: " Notes2", grade: 2, subject: [2], type: 2 },
      { id: 7, titleName: " Notes3", grade: 2, subject: [2], type: 2 },
      { id: 8, titleName: " Notes4", grade: 2, subject: [3], type: 2},
      { id: 9, titleName: " Assignemnts1", grade: 2, subject: [2], type: 3 },
      { id: 10, titleName: " Assignemnts2", grade: 1, subject: [1], type: 3 },
      { id: 11, titleName: " Assignemnts3", grade: 1, subject: [2], type: 3 },
      { id: 12, titleName: " Assignemnts4", grade: 3, subject: [3], type: 3 },
      { id: 13, titleName: " Assignemnts5", grade: 3, subject: [2], type: 3 },
      { id: 14, titleName: " Exit Passes1", grade: 3, subject: [3], type: 4 },
      { id: 15, titleName: " Exit Passes2", grade: 3, subject: [1], type: 4 },
      { id: 16, titleName: " Exit Passes3", grade: 2, subject: [1], type: 4 },
      { id: 17, titleName: " Exit Passes4", grade: 1, subject: [1], type: 4 },
      { id: 18, titleName: " Exit Passes5", grade: 3, subject: [1], type: 4 },
      { id: 19, titleName: "Notes5", grade: 4, subject: [2, 3], type: 2 },
      { id: 21, titleName: "Notes6", grade: 1, subject: [2, 3], type: 2 },
      { id: 22, titleName: "Notes7", grade: 2, subject: [1, 4], type: 2},
      { id: 23, titleName: "Notes8", grade: 1, subject: [1, 4], type: 2 },
      { id: 24, titleName: "Notes9", grade: 2, subject: [1, 4], type: 2 },
      { id: 25, titleName: "Notes10", grade: 3, subject: [1, 4], type: 2 },
      { id: 26, titleName: "Notes11", grade: 3, subject: [1, 4], type: 2 },
      { id: 27, titleName: "Notes12", grade: 5, subject: [3, 4, 5], type: 2 },
      { id: 28, titleName: "Notes13", grade: 2, subject: [1], type: 2 },
      { id: 29, titleName: "Assignemnts6", grade: 3, subject: [4], type: 3 },
      { id: 30, titleName: "Quizzes5", grade: 2, subject: [4], type: 1 },

    ]
  },
  reducers: {
    toggleSelect: (state, action) => {
      const addCardItem = {
        productId: action.payload.id,
      }
      state.cartItems.push(addCardItem)
    },
    deleteCardItem: (state, action) => {
      state.cartItems = state.cartItems.filter(item => item.productId !== action.payload.id)
    }

  },
}
)

export const {
  toggleSelect,
  deleteCardItem

} = ProductsSlice.actions;

export default ProductsSlice.reducer