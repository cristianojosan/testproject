import React from "react"
import { Link } from 'react-router-dom';

const NotFoundPage = () => {
    return(
        <div>
            <p>Ai gresit <Link to='/createaccount'>Mergi Inapoi</Link></p>
        </div>
    )
}
export default NotFoundPage