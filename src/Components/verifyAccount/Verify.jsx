import HeaderVerify from '../Header/HeaderVerify';
import { Link } from 'react-router-dom';
import './verify.scss';

const Verify = () => {
  return (

    <div className="verify-account">
      <div className="verify-container">
      <HeaderVerify />

      <div className='bg-image'>
        <div className='content'>
          <Link to="/createaccount">
            <p>Go back to your previos page</p>
            </Link>
          <div className='title'>
            <h1>Create an Account</h1>
          </div>

          <div className='verify-post '>
            <div className='verify-post-title'>
              <h3>Verify your account</h3>
            </div>
            <div className='verify-message'>
              <p>Check your inbox! Weve send a link to verify your email address and set up your account.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  )
}

export default Verify;