import './style.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import HeaderMyAccount from '../Header/HeaderMyAccount';
import { useSelector } from "react-redux";

function CreateAccount() {
  const [about, setAbout] = useState()
  const selectCampus = useSelector((state) => state.products.selectCampus)

  const handleChanges = (e) => {
    const selectedId = e.target.value
    const selectedAbout = selectCampus.filter((item) => item.id == selectedId)[0]
    setAbout(selectedAbout)
  }


  useEffect(() => {
    if (selectCampus[0]) {
      return ''

    }
    setAbout(selectCampus)
  }, [])


  const SignupSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Name is required"),
    password: Yup.string()
      .required("Password is required")
      .min(6, "Password is too short - should be 6 chars minimum"),
    confirmPassword: Yup.string().oneOf([Yup.ref('password')], 'parola nu coincide')
      .required('Confirm password is Required')
      .min(6, "Password is too short - should be 6 chars minimum"),
    email: Yup.string().email('Invalid email').required('Email is Required'),
    select: Yup.string().required('Select is required!'),
  });


  let navigate = useNavigate();
  const navigation = () => {
    navigate('/verify');
  }


  return (

    <div className="create-account">
      <div className="createaccount-container">
        <HeaderMyAccount />

        <div className='wrap'>

          <div className='content'>
            <Link to='/products'> <p className='go-previos-page'>Go back to your previos page</p></Link>

            <div className='title'>
              <h1>Create an Account</h1>
            </div>

            <Formik

              initialValues={{
                name: '',
                password: '',
                confirmPassword: '',
                email: '',
                select: '',
              }}

              validationSchema={SignupSchema}
            >
              {({ values, handleSubmit, handleChange, handleBlur, errors, touched, isValid, dirty }) => (

                <div className='form'>

                  <Form onSubmit={isValid && dirty ? navigation : handleSubmit}>
                    <div className={errors.name && touched.name ? "error-form form-group" : "form-group"}>
                      <label>Name</label>
                      <Field
                        name={'name'}
                        type={'text'}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                        className="form-control"
                        placeholder="First name" />
                    </div>
                    {errors.name && touched.name ? (<div className='error'>{errors.name}</div>) : null}

                    <div className={errors.select && touched.select ? "error-form form-group" : "form-group"}>
                      <label>Campus</label>
                      <Field
                        as="select"
                        className='select'
                        name="select"
                        value={about?.id}
                        onChange={(e) => { handleChanges(e); handleChange(e); }}
                        onBlur={handleBlur}>
                        <option disabled selected >-select-</option>
                        {selectCampus.map((item) => (
                          <option key={item.id} value={item.id}>
                            {item.name}
                          </option>
                        ))}
                      </Field>
                    </div>
                    {about ? (<div className='about'><b> ID: </b> {about?.id} <br /><b> Address: </b> {about?.address} <br /><b> Principal: </b> {about?.primary} <br /><b> Primary Contact: </b> {about?.principal}</div>) : ''}
                    {errors.select && touched.select ? (<div className='error'>{errors.select}</div>) : null}

                    <div className={errors.email && touched.email ? "error-form form-group" : "form-group"}>
                      <label>Email</label>
                      <Field
                        name={"email"}
                        type="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        className={errors.email && touched.email ? "error-form-control form-control" : "form-control"}
                        placeholder="Enter email" />

                    </div>
                    {errors.email && touched.email ? <div className='error'>{errors.email}</div> : null}

                    <div className={errors.password && touched.password ? "error-form form-group" : "form-group"}>
                      <label>Password</label>
                      <Field
                        name={'password'}
                        type="password"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        placeholder="Enter password" />
                    </div>
                    {errors.password && touched.password ? (<div className='error'>{errors.password}</div>) : null}

                    <div className={errors.confirmPassword && touched.confirmPassword ? "error-form form-group" : "form-group"}>
                      <label>Confirm Password</label>
                      <Field
                        name={'confirmPassword'}
                        type="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.confirmPassword}
                        className="form-control"
                        placeholder="Enter password" />
                    </div>
                    {errors.confirmPassword && touched.confirmPassword ? (<div className='error'>{errors.confirmPassword}</div>) : null}

                    <button type="submit" className="btn btn-primary btn-block">  Sign Up</button>

                    <br />
                    <p className="forgot-password text-right">
                      Already have an account <Link to="/login"> sign in?</Link>
                    </p>
                  </Form>

                </div>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>

  );
}

export default CreateAccount;