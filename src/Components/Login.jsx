import 'bootstrap/dist/css/bootstrap.min.css';
import { Form } from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import HeaderMyAccount from './Header/HeaderMyAccount';


function Login() {

  const SignupSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Email is Required'),
    password: Yup.string()
      .required("Password is required")
      .min(6, "Password is too short - should be 6 chars minimum"),
  });

  let navigate = useNavigate();
  const navigation = () => {
    navigate('/verify');
  }
  return (

    <div className="login">
      <div className="login-container">

        <HeaderMyAccount />

        <div className='wrap'>
          <div className='content'>
          <Link to='/createaccount'>
            <p>Go back to your previos page</p>
            </Link>
            <div className='title'>
              
              
              <h1>Create an Account</h1>
              
            </div>

            <Formik
              initialValues={{
                password: '',
                email: '',
              }}
              validationSchema={SignupSchema}
            >
              {({ values, handleSubmit, handleChange, handleBlur, errors, touched, isValid, dirty }) => (

                <div className='form'>

                  <Form onSubmit={isValid && dirty ? navigation : handleSubmit}>
                    <div className="form-group">
                      <label>Email</label>
                      <input
                        name={"email"}
                        type="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        className="form-control"
                        placeholder="Enter email" />
                    </div>
                    {errors.email && touched.email ? <div className='error'>{errors.email}</div> : null}

                    <div className="form-group">
                      <label>Password</label>
                      <input
                        name={'password'}
                        type="password"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        placeholder="Enter password" />
                    </div>
                    {errors.password && touched.password ? (<div className='error'>{errors.password}</div>) : null}

                    <button type="submit" className="btn btn-primary btn-block">Sign In</button>
                  </Form>
                </div>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>


  );
}

export default Login;
