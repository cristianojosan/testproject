import React, { useEffect } from "react"
import { useState } from 'react';
import LowmanProduct from "./LowmanProduct"
import { useSelector } from "react-redux";
import './LowmanProducts.scss';
import HeaderCreateAccount from "../Header/HeaderCreateAccount";


const LowmanProducts = () => {

  const [subjectButton, setSubjectButton] = useState(1)
  const [filteredGrades, setFilteredGrades] = useState([])
  const [filteredTypes, setFilteredTypes] = useState([])


  const grades = useSelector((state) => state.products.grades)
  const products = useSelector((state) => state.products.products)
  const cartItems = useSelector((state) => state.products.cartItems)
  const subjects = useSelector((state) => state.products.subjects)
  const types = useSelector((state) => state.products.types)


  const handleMainButtonActive = (id) => {
    setSubjectButton(id)
  }




  useEffect(() => {
    console.log('ss');
    const filterGrades = grades.map(grade => {
      return {
        grade,
        products: products.filter(product => product.grade === grade.id &&
          product.subject.includes(subjectButton)
        )
      }
    })

    const filterTypes = types.map(type => {
      return {
        type,
        products: products.filter(product => type.id === product.type &&
          product.subject.includes(subjectButton)
        )
      }
    })

    setFilteredGrades(filterGrades)
    setFilteredTypes(filterTypes)

  }, [subjectButton, products])


  console.log(filteredTypes)


  return (
    <div className="main-lowman-products">
      <div className="lowmanproducts-container">
        <HeaderCreateAccount cartItems={cartItems} />


        <div className="lowman-products-library">

          <h1>Lowman Product Library</h1>
          <div className="libraryProducts">
            {subjects.map((subject) => (
              <button
                key={subject.id}
                className={subjectButton === subject.id ? `${subject.activeBtton} ${subject.defaultBgColor}` : subject.defaultBgColor}
                onClick={() => handleMainButtonActive(subject.id)}>
                {subject.title}
              </button>
            ))}
          </div>
        </div>

        <div className="lowmanProducts ">
          {filteredGrades
            .filter((grade) => grade.products?.length)
            .map((grade) => (
              <div className="prod" key={grade.grade.id}>
                {console.log(grade)}
                <div className="productItems">

                  <h2>{grade.grade.title}</h2>

                  <div className="productItem">
                    <h5 >Profile packages</h5>
                    <button className="openButton">Open</button>
                  </div>
                  <hr />
                  {filteredTypes
                    .filter((type) => type.products?.filter((product) => product.grade === grade.grade.id).length)
                    .map(type => (
                      <div key={type.type.id}>
                        {type.products
                          .filter((product) => product.grade === grade.grade.id)
                          .map(filteredProduct => (

                            <LowmanProduct
                              key={filteredProduct.id}
                              id={filteredProduct.id}
                              selected={cartItems.find((cartItem) => cartItem.productId === filteredProduct.id)}
                              filteredProduct={filteredProduct.titleName}
                            />

                          ))}
                        <hr />
                      </div>
                    ))}
                  <div className="productItem">
                    <h5>Unit Tests</h5>
                    <button className="openButton">Open</button>
                  </div>
                  <div className="productItem">
                    <h5>Focus Package</h5>
                    <button className="openButton">Open</button>
                  </div>

                </div>
              </div>
            ))}

        </div>
      </div>
    </div>

  )

}


export default LowmanProducts
