import React from "react";
import cl from './MyModal.module.css'
import lowman from '../../../img/lowman.png'
import { AiOutlineClose } from "react-icons/ai";

const MyModal = (props) => {

    const rootClass = [cl.myModal]
    if (props.visible === true) {
        rootClass.push(cl.active);
    }

    return (
        <div className={cl.ContaIner}>
        <div className={rootClass.join(' ')} onClick={() => props.setVisible(false)}>
            <div className={cl.myModalContent} onClick={(e) => e.stopPropagation()}>
                <div className={cl.head}>
                    <h1> 6th Grade - Social Studies - Daily Quizzes</h1>
                    <i><AiOutlineClose onClick={() => props.setVisible(false)} /></i>

                </div>
                <div className={cl.cont}>

                    <div className={cl.image}>
                        <div className={cl.bigImg}>

                            <img src={lowman} />

                        </div>
                        <div className={cl.smallImg}>
                            <img src={lowman} />
                            <img src={lowman} />
                            <img src={lowman} />
                            <img src={lowman} />
                        </div>

                    </div>
                    <div className={cl.about}>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pretium ultricies nisl,  ut mollis felis fringilla vel. Donec eu elit in dui vehicula condimentum ut ac tellus. Proin imperdiet pellentesque vestibulum Proin quis lorem euismod, aliquet risus in, laoreet odio. Quisque nisl nibh, fringilla in laoreet eu, eleifend eget eros. Aenean quis eros lacinia, blandit metus eu, pulvinar tortor. Suspendisse imperdiet lorem bibendum leo auctor tempor. Nullam egestas velit quis lorem scelerisque blandit. </p>
                        <h4>Units:</h4>
                        <div className={cl.units}>
                            <ul>
                                <li>Exploration</li>
                                <li>Colonization</li>
                                <li>American Revolution</li>
                                <li>Constitution</li>
                                <li>Early Republic</li>
                                <li>Jacksonian</li>
                            </ul>
                            <ul>
                                <li>Exploration</li>
                                <li>Colonization</li>
                                <li>American Revolution</li>
                                <li>Constitution</li>
                                <li>Early Republic</li>
                                <li>Jacksonian</li>
                            </ul>
                        </div>
                        <h3>500$</h3>
                        <p>License renews annually for $50</p>
                        <button>+ ADD to order</button>
                    </div>

                </div>
                </div>
            </div>
        </div>
    )
}

export default MyModal