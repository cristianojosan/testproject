import React, { useState } from "react";
import { BsSearch, BsPlusLg, BsCheckLg } from "react-icons/bs";
import MyModal from './ModalProducts/MyModal'
import { useDispatch } from "react-redux";
import { toggleSelect } from '../../Redux/productsSlice'
import { deleteCardItem } from '../../Redux/productsSlice'





const LowmanProducts = ({ id, filteredProduct, selected }) => {
  const [modal, setModal] = useState(false)
  const [modalTitle, setModalTitle] = useState()
  const [modalName, setModalName] = useState()
  const dispatch = useDispatch()

  const handleSelectedClick = () => {

    dispatch(
      toggleSelect({ id: id })
    )

  }
  const handleDelecteClick = () => {

    dispatch(
      deleteCardItem({ id: id })
    )

  }

  return (
    <>
      <div className="productItem" >
        <div>

          <h5 className="productName">{filteredProduct}</h5>
        </div>
        <div className="addProduct">
          <div className="look" id={id}>
            <BsSearch
              onClick={(e) => { setModal(true) }} />
          </div>
          <div className="add" id={id} onClick={selected ? handleDelecteClick : handleSelectedClick}>
            {selected ? <BsCheckLg /> : !selected ? <BsPlusLg /> : <BsCheckLg />}
          </div>
        </div>

      </div>

      <MyModal
        visible={modal}
        setVisible={setModal}
        modalTitle={modalTitle}
        modalName={modalName}
      />

    </>
  )
}
export default LowmanProducts