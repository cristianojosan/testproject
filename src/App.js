import Login from './Components/Login';
import Verify from './Components/verifyAccount/Verify';
import NotFoundPage from './Components/NotFoundPage';
import CreateAccount from './Components/createAccount/CreateAccount';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './Components/Home';
import LowmanProducts from './Components/LowmanProducts/LowmanProducts';

function App() {
  return (
    <div className="app">
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/verify" element={<Verify />} />
          <Route path="/login" element={<Login />} />
          <Route path="/createaccount" element={<CreateAccount />} />
          <Route path="/products" element={<LowmanProducts />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Router>
    </div>

  );
}

export default App;
